<div align="center">

[![](https://gitlab.com/haath/haxe-hl-docker/-/raw/master/assets/logo.svg)](https://gitlab.com/haath/haxe-hl-docker)

[![](https://img.shields.io/docker/v/haath/haxe-hl?sort=semver)](https://hub.docker.com/r/haath/haxe-hl)
[![](https://img.shields.io/docker/pulls/haath/haxe-hl.svg)](https://hub.docker.com/r/haath/haxe-hl)
[![](https://img.shields.io/docker/image-size/haath/haxe-hl?sort=semver)](https://hub.docker.com/r/haath/haxe-hl)

</div>

---

Simple Docker image based on the official Haxe debian image, with [HashLink](https://hashlink.haxe.org/) installed.
The version in the Docker tag of the image corresponds to the installed HashLink version.
The base Haxe version will generally be the latest available at the time of building.

